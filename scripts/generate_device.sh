#!/bin/bash

DEVICE_CODENAME=${1}
DEVICE_NAME=${2}
DEVICE_VENDOR=${3}
DEVICE_VERSIONS=${4}
DEVICE_STATUS=${5}

DEVICE_FOLDER="_data/devices"
INFO_FOLDER="pages/info"
INSTALL_FOLDER="pages/install"
BUILD_FOLDER="pages/build"
DEVICE_PATH=${DEVICE_FOLDER}/${DEVICE_CODENAME}.yml
INFO_PATH=${INFO_FOLDER}/${DEVICE_CODENAME}.md
INSTALL_PATH=${INSTALL_FOLDER}/${DEVICE_CODENAME}.md
BUILD_PATH=${BUILD_FOLDER}/${DEVICE_CODENAME}.md
SCRIPT_ALL_DEVICES_PATH="scripts/run_all_devices.sh"

mkdir -p ${DEVICE_FOLDER}
mkdir -p ${INFO_FOLDER}
mkdir -p ${INSTALL_FOLDER}
mkdir -p ${BUILD_FOLDER}

if [ ! -f $DEVICE_PATH ]; then
  cat scripts/templates/device.yml > $DEVICE_PATH
  sed -i "s/{codename}/$DEVICE_CODENAME/g" $DEVICE_PATH
  sed -i "s/{name}/$DEVICE_NAME/g" $DEVICE_PATH
  sed -i "s/{vendor}/$DEVICE_VENDOR/g" $DEVICE_PATH
  sed -i "s/{versions}/$DEVICE_VERSIONS/g" $DEVICE_PATH
  sed -i "s/{status}/$DEVICE_STATUS/g" $DEVICE_PATH
fi

if [ ! -f $INFO_PATH ]; then
  cat scripts/templates/info.md > $INFO_PATH
  sed -i "s/{codename}/$DEVICE_CODENAME/g" $INFO_PATH
  sed -i "s/{name}/$DEVICE_NAME/g" $INFO_PATH
  sed -i "s/{vendor}/$DEVICE_VENDOR/g" $INFO_PATH
fi

if [ ! -f $INSTALL_PATH ]; then
  cat scripts/templates/install.md > $INSTALL_PATH
  sed -i "s/{codename}/$DEVICE_CODENAME/g" $INSTALL_PATH
  sed -i "s/{name}/$DEVICE_NAME/g" $INSTALL_PATH
  sed -i "s/{vendor}/$DEVICE_VENDOR/g" $INSTALL_PATH
fi

if [ ! -f $BUILD_PATH ]; then
  cat scripts/templates/build.md > $BUILD_PATH
  sed -i "s/{codename}/$DEVICE_CODENAME/g" $BUILD_PATH
  sed -i "s/{name}/$DEVICE_NAME/g" $BUILD_PATH
  sed -i "s/{vendor}/$DEVICE_VENDOR/g" $BUILD_PATH
fi

if [ ! -f $SCRIPT_ALL_DEVICES_PATH ]; then
  echo "#/bin/bash" > $SCRIPT_ALL_DEVICES_PATH
  chmod u+x $SCRIPT_ALL_DEVICES_PATH
fi

cat $SCRIPT_ALL_DEVICES_PATH | grep $DEVICE_CODENAME
if [ $? != 0 ]; then
  echo "./scripts/generate_device.sh \"${DEVICE_CODENAME}\" \"${DEVICE_NAME}\" \"${DEVICE_VENDOR}\" \"${DEVICE_VERSIONS}\" \"${DEVICE_STATUS}\"" >> scripts/run_all_devices.sh
fi
