---
sidebar: home_sidebar
title: Install /e/ on Samsung Galaxy S4 Mini (International Dual SIM) - serranodsdd
folder: install
layout: default
permalink: /devices/serranodsdd/install
device: serranodsdd
---
{% include templates/device_install.md %}
