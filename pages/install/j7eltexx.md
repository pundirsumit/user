---
sidebar: home_sidebar
title: Install /e/ on Samsung Galaxy J7 (Exynos) - j7eltexx
folder: install
layout: default
permalink: /devices/j7eltexx/install
device: j7eltexx
---
{% include templates/device_install.md %}
