---
sidebar: home_sidebar
title: Install /e/ on Google Pixel - sailfish
folder: install
layout: default
permalink: /devices/sailfish/install
device: sailfish
---
{% include templates/device_install.md %}
