---
sidebar: home_sidebar
title: Install /e/ on Google Nexus 5 - hammerhead
folder: install
layout: default
permalink: /devices/hammerhead/install
device: hammerhead
---
{% include templates/device_install.md %}
