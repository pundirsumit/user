---
sidebar: home_sidebar
title: Install /e/ on Samsung Galaxy S9 - starlte
folder: install
layout: default
permalink: /devices/starlte/install
device: starlte
---
{% include templates/device_install.md %}
