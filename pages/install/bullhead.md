---
sidebar: home_sidebar
title: Install /e/ on Google Nexus 5X - bullhead
folder: install
layout: default
permalink: /devices/bullhead/install
device: bullhead
---
{% include templates/device_install.md %}
