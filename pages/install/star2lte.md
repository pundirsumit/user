---
sidebar: home_sidebar
title: Install /e/ on Samsung Galaxy S9+ - star2lte
folder: install
layout: default
permalink: /devices/star2lte/install
device: star2lte
---
{% include templates/device_install.md %}
