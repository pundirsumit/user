---
sidebar: home_sidebar
title: Install /e/ on Samsung Galaxy S4 Mini (International 3G) - serrano3gxx
folder: install
layout: default
permalink: /devices/serrano3gxx/install
device: serrano3gxx
---
{% include templates/device_install.md %}
