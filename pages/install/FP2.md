---
sidebar: home_sidebar
title: Install /e/ on FairPhone FP2 - FP2
folder: install
layout: default
permalink: /devices/FP2/install
device: FP2
---
{% include templates/device_install.md %}
