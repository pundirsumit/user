---
sidebar: home_sidebar
title: Install /e/ on OnePlus 3/3T - oneplus3
folder: install
layout: default
permalink: /devices/oneplus3/install
device: oneplus3
---
{% include templates/device_install.md %}
