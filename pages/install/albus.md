---
sidebar: home_sidebar
title: Install /e/ on Motorola Moto Z2 Play - albus
folder: install
layout: default
permalink: /devices/albus/install
device: albus
---
{% include templates/device_install.md %}
