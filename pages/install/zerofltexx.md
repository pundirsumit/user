---
sidebar: home_sidebar
title: Install /e/ on Samsung Galaxy S6 - zerofltexx
folder: install
layout: default
permalink: /devices/zerofltexx/install
device: zerofltexx
---
{% include templates/device_install.md %}
