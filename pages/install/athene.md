---
sidebar: home_sidebar
title: Install /e/ on Motorola Moto G4 - athene
folder: install
layout: default
permalink: /devices/athene/install
device: athene
---
{% include templates/device_install.md %}
