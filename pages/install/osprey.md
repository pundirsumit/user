---
sidebar: home_sidebar
title: Install /e/ on Motorola Moto G 2015 - osprey
folder: install
layout: default
permalink: /devices/osprey/install
device: osprey
---
{% include templates/device_install.md %}
