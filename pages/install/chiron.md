---
sidebar: home_sidebar
title: Install /e/ on Xiaomi Mi MIX 2 - chiron
folder: install
layout: default
permalink: /devices/chiron/install
device: chiron
---
{% include templates/device_install.md %}
