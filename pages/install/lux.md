---
sidebar: home_sidebar
title: Install /e/ on Motorola Moto X Play - lux
folder: install
layout: default
permalink: /devices/lux/install
device: lux
---
{% include templates/device_install.md %}
