---
sidebar: home_sidebar
title: Install /e/ on Samsung Galaxy S4 - jfltexx
folder: install
layout: default
permalink: /devices/jfltexx/install
device: jfltexx
---
{% include templates/device_install.md %}
