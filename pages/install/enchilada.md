---
sidebar: home_sidebar
title: Install /e/ on OnePlus 6 - enchilada
folder: install
layout: default
permalink: /devices/enchilada/install
device: enchilada
---
{% include templates/device_install.md %}
