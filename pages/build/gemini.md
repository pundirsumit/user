---
sidebar: home_sidebar
title: Build /e/ for Xiaomi Mi 5 - gemini
folder: build
layout: default
permalink: /devices/gemini/build
device: gemini
---
{% include templates/device_build.md %}
