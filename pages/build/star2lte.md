---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy S9+ - star2lte
folder: build
layout: default
permalink: /devices/star2lte/build
device: star2lte
---
{% include templates/device_build.md %}
