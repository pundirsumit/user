---
sidebar: home_sidebar
title: Build /e/ for ZTE Axon7 - axon7
folder: build
layout: default
permalink: /devices/axon7/build
device: axon7
---
{% include templates/device_build.md %}
