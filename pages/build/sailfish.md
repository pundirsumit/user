---
sidebar: home_sidebar
title: Build /e/ for Google Pixel - sailfish
folder: build
layout: default
permalink: /devices/sailfish/build
device: sailfish
---
{% include templates/device_build.md %}
