---
sidebar: home_sidebar
title: Build /e/ for HTC One (M8) - m8
folder: build
layout: default
permalink: /devices/m8/build
device: m8
---
{% include templates/device_build.md %}
