---
sidebar: home_sidebar
title: Build /e/ for Samsung Galaxy J5 LTE - j5lte
folder: build
layout: default
permalink: /devices/j5lte/build
device: j5lte
---
{% include templates/device_build.md %}
