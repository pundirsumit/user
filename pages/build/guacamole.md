---
sidebar: home_sidebar
title: Build /e/ for OnePlus 7 Pro - guacamole
folder: build
layout: default
permalink: /devices/guacamole/build
device: guacamole
---
{% include templates/device_build.md %}
