---
layout: page
title: Jobs @/e/
permalink: jobs
search: exclude
---

## Currently opened positions (remote):

Those are full time positions. 

 - [PHP] we are looking for PHP developer with experience, to work on various online services components, improve them, add some features, debug... Passionate, hacker in mind, happy to work as part of a multi-cultural team, for a great project like /e/!
 - [JAVA] we are looking for a java developer for low-level development linked to Android and API reverse engineering. Hacker in mind, loves to solve complex cases.
 - [DEVOPS] We are looking for an infrastructure engineer who will be able to help us integrate and deploy a scalable architecture for hosting our email, drive, calendar, ... /e/ services on the top of existing software solutions. 
Some experience in this field is required, and must have knowledge in state of the art security practices.

## How to apply?

Please send an email to <jobs@e.email> with a resume.
