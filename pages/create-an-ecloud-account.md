---
layout: page
title: Create an /e/ account
permalink: create-an-ecloud-account
search: exclude
---

In order to benefit from /e/ online services and single identity in the /e/ operating system (single identity for email, drive, calendar, notes and tasks), you have to use an /e/ account.

-> [Register here for your free account](https://e.foundation/e-mail-invite/)

Your free account will take the form of: user@e.email / password. User@e.email will be your newly created /e/ email address.

**Important note:** your free account will be **limited to 5GB** online storage. We now have some premium storage plans available. You can also **upgrade it to 20GB** while supporting the /e/ project by becoming an [/e/ Early Adopter](https://e.foundation/donate/).

**PLEASE KEEP IN MIND** that /e/ Free drive and mail accounts are supported by donations! Please [support us now and receive a gift](https://e.foundation/support-us/).

<i>"Create /e/ free account" is licensed under [CC BY](https://creativecommons.org/licenses/by-sa/3.0/) by e Foundation 2018-2019.</i>
