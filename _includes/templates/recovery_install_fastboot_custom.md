## Unlocking the bootloader

{% include alerts/warning_unlocking_bootloader.md %}

1. Unlock your bootloader by following [this]({{ device.unlock_bootloader_guide }}) guide.

{% include templates/recovery_install_fastboot_generic.md %}
