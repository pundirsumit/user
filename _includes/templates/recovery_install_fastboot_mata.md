## Notes

Some useful additional information can be found at:
1. https://forum.xda-developers.com/essential-phone/development/rom-lineageos-14-1-essential-ph-1-mata-t3703759
1. https://forum.xda-developers.com/essential-phone/development/stock-7-1-1-nmj20d-t3701681
1. https://mata.readthedocs.io/en/latest/

## Unlocking the bootloader

1. Unlock bootloader

    ```shell
    fastboot flashing unlock
    ```

1. Unlock critical partition

    ```shell
    fastboot flashing unlock_critical
    ```

## Flashing firmwares

1. Download the [NMJ88C archive](https://mega.nz/#!1CgF3SLa!CHLLA0HvtDtzzDnOl3CbJ1EUnhR6vFb4NA9IG6au3to) and unzip it

1. Reboot in bootloader mode

    ```shell
    adb reboot bootloader
    ```

1. execute the script:

    On Linux or MacOS X:

    ```shell
    ./flash-all.sh
    ```

    On Windows:

    ```shell
    ./flash-all.bat
    ```

(it takes a while to complete)

## Installing a custom recovery: TWRP for Mata

1. Download [TWRP for Mata](https://androidfilehost.com/?fid=818070582850502775)
1. Connect your device to your PC via USB.
1. Flash TWRP to the boot partition:

    ```shell
    fastboot -w
    fastboot flash boot twrp-mata_11.img
    ```
1. Stay in bootloader mode, and use the volume down to select "Recovery mode". Then  hit the power button: this should boot you in to recovery mode (TWRP). If it's asking for a password, enter your previous lockscreen password or pin, this will decrypt your data partition. Note: don't be surprised that TWRP will be erased at next boot. Therefore, you will have to reinstall it if you want to install another ROM on your device.
