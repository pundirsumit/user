## Installing a custom recovery

1. Download a custom recovery - you can download [TWRP](https://dl.twrp.me/{{ device.codename }}).
1. Connect your device to your PC via USB.
1. On the computer, open a command prompt (on Windows) or terminal (on Linux or macOS) window, and type

    ```shell
    adb reboot bootloader
    ```
1. Once the device is in fastboot mode, verify your PC finds it by typing

    ```shell
    fastboot devices
    ```
1. Flash recovery onto your device

    ```shell
    fastboot flash recovery twrp-x.x.x-x-{{ device.codename }}.img
    ```
1. Now reboot into recovery to verify the installation
