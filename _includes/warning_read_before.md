>**Very important: please read the following carefully before proceeding!**
>
> Installing a new operating system on a mobile device can potentially:
> 1. lead to all data destruction on the device
> 1. make it an unrecoverable brick.
>
> So please **only** flash your device if you **know** what you are doing and are OK with taking the **associated risk**.
>
> The /e/ project and its project members deny any and all responsibility about the consequences of using /e/ software and/or /e/ services.
